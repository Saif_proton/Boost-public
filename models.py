
from sqlalchemy import create_engine, Column, Integer, String,Text,DateTime,Boolean,ForeignKey,Date
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session,relationship
Base = declarative_base()


class NetworkOperator(Base):

    __tablename__ = 'network_operator'

    operatorId = Column(Integer, primary_key=True, autoincrement=True)
    operatorName = Column(String(50))
    countryCode = Column(String(45))

    user_info = relationship("UserInfo", back_populates="network_operator")



class UserInfo(Base):

    __tablename__ = 'user_info'

    userId = Column(Integer, primary_key=True, autoincrement=True)
    operatorId = Column(Integer, ForeignKey('network_operator.operatorId'))
    firstName = Column(String(50))
    lastName = Column(String(50))
    email = Column(String(100))
    mobileNumber = Column(String(45))
    deviceToken = Column(String(256))
    preferredLanguage = Column(String(45))
    otp = Column(String(200))
    pin = Column(String(200))
    createdAt = Column(DateTime)
    updatedAt = Column(DateTime)

    network_operator = relationship("NetworkOperator", back_populates="user_info")
    account_details = relationship("AccountDetails", back_populates="user_info")
    login_info = relationship("LoginInfo", back_populates="user_info")
    login_logs = relationship("LoginLogs", back_populates="user_info")
    user_kyc = relationship("UserKYC", back_populates="user_info")


class BankName(Base):
    __tablename__ = 'bank_name'
    bankId = Column(Integer, primary_key=True, autoincrement=True)
    bankName = Column(String(200))
    bankImage = Column(String(100))

    account_details = relationship("AccountDetails", back_populates="bank_name")

class AccountDetails(Base):

    __tablename__ = 'account_details'

    accountDetailId = Column(Integer, primary_key=True, autoincrement=True)
    userId = Column(Integer, ForeignKey('user_info.userId'))
    bankId = Column(Integer, ForeignKey('bank_name.bankId'))
    accountTitle = Column(String(45))
    IBAN = Column(String(45))
    createdAt = Column(DateTime)

    user_info = relationship("UserInfo", back_populates="account_details")
    bank_name = relationship("BankName", back_populates="account_details")
    borrow_loans = relationship("BorrowLoan", back_populates="account_details")

class BorrowLoan(Base):

    __tablename__ = 'borrow_loan'

    loanId = Column(Integer, primary_key=True, autoincrement=True)
    accountDetailId = Column(Integer, ForeignKey('account_details.accountDetailId'))
    amountBorrow = Column(Integer)
    repayableAmount = Column(String(45))
    applicationDate = Column(Date)
    repaymentDays = Column(Integer)
    repaymentDate = Column(Date)
    approvedDate = Column(Date)
    status = Column(String(45))
    isReleased = Column(Boolean)
    createdAt = Column(DateTime)

    account_details = relationship("AccountDetails", back_populates="borrow_loans")
    pay_backs = relationship("PayBack", back_populates="borrow_loan")

class EmploymentDetails(Base):

    __tablename__ = 'employment_details'

    employmentDetailId = Column(Integer, primary_key=True, autoincrement=True)
    kycId = Column(Integer, ForeignKey('user_kyc.kycId'))
    companyName = Column(String(45))
    designation = Column(String(45))
    dateOfJoining = Column(Date)
    salarySlipPic = Column(String(200))
    createdAt = Column(DateTime)

    user_kyc = relationship("UserKYC", back_populates="employment_details")

class PayBack(Base):

    __tablename__ = 'pay_back'

    payBackId = Column(Integer, primary_key=True, autoincrement=True)
    loanId = Column(Integer, ForeignKey('borrow_loan.loanId'))
    payBackStatus = Column(String(45))
    receiptPic = Column(String(200))
    paidAt = Column(DateTime)
    createdAt = Column(DateTime)

    borrow_loan = relationship("BorrowLoan", back_populates="pay_backs")

class StudentInfo(Base):

    __tablename__ = 'student_info'

    studentId = Column(Integer, primary_key=True, autoincrement=True)
    kycId = Column(Integer, ForeignKey('user_kyc.kycId'))
    studentType = Column(String(45))
    instituteName = Column(String(45))
    enrolmentDate = Column(Date)
    studentCardPic = Column(String(200))
    createdAt = Column(DateTime)

    user_kyc = relationship("UserKYC", back_populates="student_info")

class UserKYC(Base):

    __tablename__ = 'user_kyc'

    kycId = Column(Integer, primary_key=True, autoincrement=True)
    userId = Column(Integer, ForeignKey('user_info.userId'))
    employmentType = Column(String(45))
    qualification = Column(String(45))
    address = Column(String(200))
    maritalStatus = Column(String(45))
    children = Column(String(45))
    cnicPic = Column(String(200))
    cnicNo = Column(String(45))
    dateOfBirth = Column(Date)
    dateOfCnicExpiry = Column(String(45))
    facePicture = Column(String(200))
    createdAt = Column(DateTime)
    updatedAt = Column(DateTime)

    user_info = relationship("UserInfo", back_populates="user_kyc")
    employment_details = relationship("EmploymentDetails", back_populates="user_kyc")
    student_info = relationship("StudentInfo", back_populates="user_kyc")

class LoginInfo(Base):

    __tablename__ = 'login_info'

    loginId = Column(Integer, primary_key=True, autoincrement=True)
    userId = Column(Integer, ForeignKey('user_info.userId'))
    isPin = Column(Boolean)
    isTouch = Column(Boolean)
    isFace = Column(Boolean)
    loginTime = Column(DateTime)
    logoutTime = Column(DateTime)
    createdAt = Column(DateTime)

    user_info = relationship("UserInfo", back_populates="login_info")

class LoginLogs(Base):

    __tablename__ = 'login_logs'

    logId = Column(Integer, primary_key=True, autoincrement=True)
    userId = Column(Integer, ForeignKey('user_info.userId'))
    isPin = Column(Boolean)
    isTouch = Column(Boolean)
    isFace = Column(Boolean)
    loginTime = Column(DateTime)
    status = Column(String(45))
    createdAt = Column(DateTime)

    user_info = relationship("UserInfo", back_populates="login_logs")