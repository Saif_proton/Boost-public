from fastapi import FastAPI, HTTPException, Depends, File,Form, status,Request,UploadFile
from pydantic import BaseModel, field_validator
from pathlib import Path
import json
from sqlalchemy import create_engine,case, literal,select,and_,func
from sqlalchemy.sql.functions import coalesce
from sqlalchemy.orm import sessionmaker, Session,relationship,aliased
import configparser
from typing import List,Dict,Optional,Any
import random
import os
from fastapi.middleware.cors import CORSMiddleware
import jwt
import json
import re
from datetime import datetime, timedelta,date
from fastapi.responses import JSONResponse,FileResponse
from base64 import b64encode,b64decode
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad,unpad
from fastapi.security import OAuth2PasswordBearer
import uvicorn
import models
import bcrypt
from werkzeug.utils import secure_filename

app = FastAPI(docs_url=None,redoc_url=None)
app.add_middleware(CORSMiddleware,allow_origins=["*"], allow_credentials=True,allow_methods=["*"],allow_headers=["*"])

config = configparser.ConfigParser()
config.read("config.ini")

# SQLAlchemy setup
DATABASE_URL = config.get("database", "url")
engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


class EncryptResponseModel(BaseModel):
    edata: dict


class DecryptResponseModel(BaseModel):
    code: int
    status: str
    message: str
    data: dict


class DecryptModel(BaseModel):
    ciphertext: str


class DecryptInput(BaseModel):
    data: str

class UserCredentials(BaseModel):
    username: str
    password: str


class UserRegistrationStep1(BaseModel):
    mobileNumber: str
    deviceToken: str
    isUrdu: bool


class UserRegistrationStep2(BaseModel):
    mobileNumber: str
    otp: str
    isUrdu: bool


class UserRegistration(BaseModel):
    countryCode: str
    operatorName: Optional[str] = None
    firstName: str
    lastName: str
    email: Optional[str] = None
    mobileNumber: str
    deviceToken: str
    otp: str
    pin: str
    isUrdu: bool


class UserLogin(BaseModel):
    userId: int
    pin: Optional[str] = None
    preferredLanguage: Optional[str] = None
    isPin: bool
    isTouch: bool
    isFace: bool
    isUrdu: bool


class UserLogout(BaseModel):
    userId: int
    isUrdu: bool

class UserLanguagae(BaseModel):
    userId: int
    preferredLanguage: str
    isUrdu: bool
class UserPreference(BaseModel):
    isUrdu: bool

class VerifyOTP(BaseModel):
    userId: int
    otp: str
    isUrdu: bool

class ChangePin(BaseModel):
    userId: int
    pin: str
    isUrdu: bool

class UserKyc(BaseModel):
    userId: int
    isUrdu: bool
    education: str
    address: str
    maritalStatus: str
    children: Optional[str] = None
    employmentStatus: str
    studentType: Optional[str] = None
    instituteName: Optional[str] = None
    enrollmentDate: Optional[date] = None
    companyName: Optional[str] =None
    designation: Optional[str] =None
    dateOfJoining: Optional[date] =None
    cnicNumber: str
    cnicDateOfBirth: date
    cnicDateOfExpiry: date


class LoanAmount(BaseModel):
    userId: int
    amount: int
class SubmitLoan(BaseModel):
    userId: int
    isUrdu: bool
    amount: int
    daysToReturnLoan: int
    accountId: Optional[int] = None
    bankId: Optional[int] = None
    accountTitle: Optional[str] = None
    ibanNumber: Optional[str] = None

class LoanDetail(BaseModel):
    userId: int

class PayBack(BaseModel):
    loanId: int
    isUrdu: bool

SECRET_KEY = 'b0ostAzadi!234'
ALGORITHM = 'HS256'
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/api/auth/v1/TokenGenerate")


def create_access_token(data: dict, expires_delta: timedelta):
    to_encode = data.copy()
    expire = datetime.utcnow() + expires_delta
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode,SECRET_KEY,ALGORITHM)
    return encoded_jwt


def validate_token(token: str = Depends(oauth2_scheme)):
    try:
        payload = jwt.decode(token,SECRET_KEY,ALGORITHM)
        return True, payload
    except jwt.ExpiredSignatureError:
        return False, "Token has expired"
    except jwt.DecodeError:
        return False, "Invalid token"

# this is a helper function to generate a cipher text of input parameters
def encrypt_input(data):
    temp ={}
    data = json.dumps(data).encode('utf-8')
    key = b'pplfe775xvye8j81elpo9b14d9c09098'
    iv = b'fpmjlrbhpljoennm'
    cipher = AES.new(key, AES.MODE_CBC,iv)
    ct_bytes = cipher.encrypt(pad(data, AES.block_size,style='pkcs7'))
    ct = b64encode(ct_bytes).decode('utf-8')
    temp['data'] = ct
    return temp

def decrypt_input(data):
    key = b'pplfe775xvye8j81elpo9b14d9c09098'
    iv =b'fpmjlrbhpljoennm'
    ciphertext = data
    ct = b64decode(ciphertext)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    pt = unpad(cipher.decrypt(ct), AES.block_size,style='pkcs7')
    pt_str = pt.decode('utf-8')

    try:
        pt = json.loads(pt_str)
        return JSONResponse(content=pt,status_code=200)
    except:
        return JSONResponse(content=pt_str, status_code=200)




def encrypt_data(data:DecryptResponseModel):
    response={}
    temp ={}
    data = json.dumps(data).encode('utf-8')
    key = b'pplfe775xvye8j81elpo9b14d9c09098'
    iv = b'fpmjlrbhpljoennm'
    cipher = AES.new(key, AES.MODE_CBC,iv)
    ct_bytes = cipher.encrypt(pad(data, AES.block_size,style='pkcs7'))
    ct = b64encode(ct_bytes).decode('utf-8')
    temp['ciphertext'] = ct
    response['edata'] = temp
    return response


def generate_otp():
    return str(random.randint(1000, 9999))


def validate_phone_number(phone):
    cleaned_phone = re.sub(r'^\+?92(0)?|^0', '', phone)
    if re.match(r'^\d{10}$', cleaned_phone):
        return cleaned_phone
    else:
        return None


def decrypt_and_decode(field_name):
    field_name = decrypt_input(field_name)
    return field_name.body.decode().strip('"')
async def save_uploaded_file(uploaded_file: UploadFile, file_path: str):
    with open(file_path, 'wb') as file:
        file.write(uploaded_file.file.read())


bank_logos_directory = Path("BankLogos")

@app.get("/BankLogo/{image_filename}")
async def get_bank_logo(image_filename: str):
    image_path = bank_logos_directory / image_filename
    if not image_path.is_file():
        return JSONResponse(content={"error": "Image not found"}, status_code=404)
    return FileResponse(image_path, media_type="image/jpeg")

@app.post("/api/auth/v1/Decrypt",response_model=DecryptResponseModel)
def decrypt_data(data:DecryptModel,payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            key = b'pplfe775xvye8j81elpo9b14d9c09098'
            iv =b'fpmjlrbhpljoennm'
            ciphertext = data.ciphertext
            ct = b64decode(ciphertext)
            cipher = AES.new(key, AES.MODE_CBC, iv)
            pt = unpad(cipher.decrypt(ct), AES.block_size,style='pkcs7')
            pt = json.loads(pt.decode('utf-8'))
            if isinstance(pt, str):
                pt = json.loads(pt)
            return JSONResponse(content=pt,status_code=200)
        except (ValueError, KeyError):
            respone = {
            'code': 401,
            'status': 'Fail',
            'message':'Invalid Decryption',
            'data': {}
            }
            return JSONResponse(content= respone,status_code=401)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
        return JSONResponse(content=response_data, status_code=401)


@app.post("/api/auth/v1/TokenGenerate",response_model=DecryptResponseModel)
def token_required(input: DecryptInput):
    try:
        data = decrypt_input(input.data)
        data = json.loads(data.body.decode())
        data = json.dumps(data)
        data = UserCredentials.model_validate_json(data)

        user_email = data.username
        user_password = data.password

        if not user_email or not user_password:
            response = {'status': False,'message': 'email or password is missing','code': 400,'data': {}}
            return JSONResponse(content=response, status_code=400)
        user = 'info@boost.com'
        password = 'Boxst123'
        if user != user_email or password != user_password:
            response = {'status': False, 'message': 'Invalid email or password', 'code': 401, 'data': {}}
            return JSONResponse(content=response, status_code=401)

        access_token = create_access_token({"sub": user}, timedelta(minutes=60))

        response = {
            'status': True,
            'message': 'Success',
            'code': 200,
            'data': {
                "access_token": access_token,
                "token_type": "bearer",
                "expires_in": "3600"
            }
        }
        return JSONResponse(content=response, status_code=200)
    except Exception as e:
        response = {
            "code": 400,
            "status": "Fail",
            "message": str(e),
            "data": {}
        }
        return JSONResponse(content=response, status_code=400)

@app.post("/api/auth/v1/encryptJsonInput")
async def testing(input:Request):
    try:
        json_body = await input.json()
        enc = encrypt_input(json_body)
        response = {
            'status': True,
            'message': 'Success',
            'code': 200,
            'data': enc
        }
        return JSONResponse(content=response, status_code=200)
    except Exception as e:
        response = {
            "code": 400,
            "status": "Fail",
            "message": str(e),
            "data": {}
        }
        return JSONResponse(content=response, status_code=400)




otp_storage = {}
@app.post("/api/auth/v1/step1/register", response_model=EncryptResponseModel)
def register_userStep1(input:DecryptInput, payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = UserRegistrationStep1.model_validate_json(data)

            cleaned_phone = validate_phone_number(user.mobileNumber)
            if cleaned_phone:
                db = SessionLocal()
                existing_user = db.query(models.UserInfo).filter(models.UserInfo.mobileNumber == cleaned_phone).first()
                if existing_user:
                    db.query(models.UserInfo).filter(models.UserInfo.userId == existing_user.userId).update({'deviceToken': user.deviceToken,'updatedAt':datetime.now()})
                    db.commit()

                    if user.isUrdu:
                        msg = "صارف پہلے ہی رجسٹرڈ ہے۔"
                    else:
                        msg = "User already registered"

                    response_data = {
                        "code": 200,
                        "status": "Success",
                        "message": msg,
                        "data": {"isRegistered": 1,"userId": existing_user.userId,
                                 "firstName": existing_user.firstName,"lastName":existing_user.lastName,
                                 "mobileNumber": "+92" + str(existing_user.mobileNumber)}
                    }
                else:
                    otp = generate_otp()
                    otp_storage[cleaned_phone] = otp

                    # write code to send sms on user mobile number

                    if user.isUrdu:
                        msg = f"OTP {user.mobileNumber} پر بھیجا گیا ہے "

                    else:
                        msg = f"OTP has been sent to {user.mobileNumber}"

                    response_data = {
                        "code": 200,
                        "status": "Success",
                        "message": msg,
                        "data": {"isRegistered": 0,"OTP": otp}
                    }
            else:
                if user.isUrdu:
                    msg = "ناموزوں نمبر"
                else:
                    msg = "Invalid Number"

                response_data = {
                    "code": 400,
                    "status": "Fail",
                    "message": msg,
                    "data": {}
                }
        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)

    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)



@app.post("/api/auth/v1/step2/register", response_model=EncryptResponseModel)
def register_userStep2(input:DecryptInput, payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = UserRegistrationStep2.model_validate_json(data)

            cleaned_phone = validate_phone_number(user.mobileNumber)
            if cleaned_phone:
                if cleaned_phone in otp_storage:
                    stored_otp = otp_storage[cleaned_phone]
                    #if user.otp == stored_otp:
                    if user.isUrdu:
                        msg = " کامیابی سے تصدیق ہو گئی"
                    else:
                        msg = "OTP Verified Successfully"
                    response_data = {
                        "code": 200,
                        "status": "Success",
                        "message": msg,
                        "data": {"message": msg}
                    }
                    # else:
                    #     if user.isUrdu:
                    #         msg = "غلط OTP"
                    #     else:
                    #         msg = "Invalid OTP"
                    #     response_data = {
                    #         "code": 400,
                    #         "status": "Fail",
                    #         "message": msg,
                    #         "data": {}
                    #     }
                else:
                    response_data = {
                        "code": 400,
                        "status": "Fail",
                        "message": "OTP not found",
                        "data": {}
                    }
            else:
                if user.isUrdu:
                    msg =  "ناموزوں نمبر"
                else:
                    msg = "Invalid Number"
                response_data = {
                    "code": 400,
                    "status": "Fail",
                    "message": msg,
                    "data": {}
                }
        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)


@app.post("/api/auth/v1/step3/register", response_model=EncryptResponseModel)
def register_userStep3(input:DecryptInput, payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        data = decrypt_input(input.data)
        data = json.loads(data.body.decode())
        data = json.dumps(data)
        user = UserRegistration.model_validate_json(data)

        user.countryCode = user.countryCode.lower()
        if user.operatorName:
            user.operatorName = user.operatorName.lower()
        user.mobileNumber = validate_phone_number(user.mobileNumber)
        otp = user.otp
        if user.mobileNumber in otp_storage:
            del otp_storage[user.mobileNumber]
        hashed_otp = bcrypt.hashpw(otp.encode("utf-8"), bcrypt.gensalt())
        hashed_pin = bcrypt.hashpw(user.pin.encode("utf-8"), bcrypt.gensalt())
        db = SessionLocal()
        try:
            network_operator = models.NetworkOperator(
                operatorName=user.operatorName,
                countryCode=user.countryCode
            )
            db.add(network_operator)
            db.commit()

            # Create a UserInfo instance
            user_info = models.UserInfo(
                operatorId=network_operator.operatorId,
                firstName=user.firstName,
                lastName=user.lastName,
                email=user.email,
                mobileNumber=user.mobileNumber,
                deviceToken=user.deviceToken,
                pin=hashed_pin,
                otp=hashed_otp,
                createdAt=datetime.now()
            )
            db.add(user_info)
            db.commit()

            if user.isUrdu:
                msg = "صارف کامیابی کے ساتھ رجسٹر ہو گیا ہے۔"
            else:
                msg = "User registered successfully"
            response_data = {
                "code": 200,
                "status": "Success",
                "message": msg,
                "data": {"message": msg}
            }

        except Exception as e:
            db.rollback()
            if user.isUrdu:
                msg = "صارف کی رجسٹریشن کے دوران ایک خرابی واقع ہوئی"
            else:
                msg = "An error occurred while registering user"
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": msg,
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)



@app.post("/api/auth/v1/login", response_model=EncryptResponseModel)
def user_login(input:DecryptInput , payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    db = SessionLocal()
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = UserLogin.model_validate_json(data)

            existing_user = db.query(models.UserInfo).filter(models.UserInfo.userId == user.userId).first()
            if existing_user:
                if user.isPin:
                    if user.pin:
                        if not bcrypt.checkpw(user.pin.encode("utf-8"), existing_user.pin.encode("utf-8")):

                            login_logs = models.LoginLogs(
                                userId=user.userId,
                                isPin=user.isPin,
                                isTouch=user.isTouch,
                                isFace=user.isFace,
                                loginTime=datetime.now(),
                                status="False",
                                createdAt= datetime.now()

                            )
                            db.add(login_logs)
                            db.commit()

                            if user.isUrdu:
                                msg = "غلط پن"
                            else:
                                msg = "incorrect pin"
                            response_data = {
                                "code": 400,
                                "status": "Fail",
                                "message": msg,
                                "data": {}
                            }
                            encrypt_response = encrypt_data(response_data)
                            return JSONResponse(content=encrypt_response, status_code=400)

                    else:
                        response_data = {
                            "code": 400,
                            "status": "Fail",
                            "message": "PIN is required when isPin is True",
                            "data": {}
                        }
                        encrypt_response = encrypt_data(response_data)
                        return JSONResponse(content=encrypt_response, status_code=400)

                if user.preferredLanguage:
                    user.preferredLanguage = user.preferredLanguage.lower()
                    db.query(models.UserInfo).filter(models.UserInfo.userId == user.userId).update({'preferredLanguage': user.preferredLanguage,'updatedAt':datetime.now()})
                    db.commit()

                login_logs = models.LoginLogs(
                    userId=user.userId,
                    isPin=user.isPin,
                    isTouch=user.isTouch,
                    isFace=user.isFace,
                    loginTime=datetime.now(),
                    status="True",
                    createdAt=datetime.now()

                )
                db.add(login_logs)
                db.commit()

                login = models.LoginInfo(
                    userId=user.userId,
                    isPin=user.isPin,
                    isTouch=user.isTouch,
                    isFace=user.isFace,
                    loginTime=datetime.now(),
                    createdAt=datetime.now()

                )
                db.add(login)
                db.commit()
                outstanding = 0
                loan_availed = 0
                existing_user = db.query(models.UserInfo).filter(models.UserInfo.userId == user.userId).first()
                user_kyc = db.query(models.UserKYC.userId).filter(models.UserKYC.userId == user.userId).first()
                results = db.query(models.BorrowLoan.amountBorrow, models.BorrowLoan.repayableAmount, models.BorrowLoan.repaymentDate,coalesce(models.PayBack.payBackStatus, 'pending').label('payBackStatus'), models.BorrowLoan.isReleased).join(models.AccountDetails, models.BorrowLoan.accountDetailId == models.AccountDetails.accountDetailId).outerjoin(models.PayBack, models.PayBack.loanId == models.BorrowLoan.loanId).filter(models.AccountDetails.userId == user.userId, models.BorrowLoan.status == 'paid').all()
                if user_kyc:
                    isKyc = 1
                else:
                    isKyc = 0
                for result in results:
                    amount_borrow, repayable_amount,repaymentDate, status, is_released = result

                    # Calculate outstanding
                    if status == 'pending':
                        outstanding += int(repayable_amount)

                    # Calculate loan availed
                    if status == 'paid':
                        loan_availed += int(amount_borrow)

                if user.isUrdu:
                    msg = "لاگ ان کامیاب"
                else:
                    msg = "Login Successful"
                response_data = {
                    "code": 200,
                    "status": "Success",
                    "message": msg,
                    "data": {"userId": existing_user.userId,"firstName":existing_user.firstName,"lastName":existing_user.lastName, "mobileNumber":existing_user.mobileNumber,"isKyc":isKyc,"availableLoanLimit":3000,"loanAvailed":loan_availed,"loanOutstanding":outstanding}
                }
                encrypt_response = encrypt_data(response_data)
                return JSONResponse(content=encrypt_response, status_code=200)

            else:
                response_data = {
                    "code": 400,
                    "status": "Fail",
                    "message": "User not Registered",
                    "data": {}
                }
                encrypt_response = encrypt_data(response_data)
                return JSONResponse(content=encrypt_response, status_code=400)

        except Exception as e:

            login_logs = models.LoginLogs(
                userId=user.userId,
                isPin=user.isPin,
                isTouch=user.isTouch,
                isFace=user.isFace,
                loginTime=datetime.now(),
                status= "error occured",
                createdAt=datetime.now()

            )
            db.add(login_logs)
            db.commit()

            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)


@app.post("/api/auth/v1/logout", response_model=EncryptResponseModel)
def user_logout(input : DecryptInput, payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = UserLogout.model_validate_json(data)

            db = SessionLocal()
            latest_login = db.query(models.LoginInfo).filter(models.LoginInfo.userId == user.userId).order_by(models.LoginInfo.loginTime.desc()).first()
            if latest_login:
                latest_login.logoutTime = datetime.now()
                db.commit()

            if user.isUrdu:
                msg = "صارف کامیابی سے لاگ آؤٹ ہو گیا ہے۔"
            else:
                msg = "Logout Successfully"
            response_data = {
                "code": 200,
                "status": "Success",
                "message": msg,
                "data": {"message": msg}
            }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)



@app.post("/api/auth/v1/changeLanguage", response_model=EncryptResponseModel)
def change_language(input:DecryptInput, payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = UserLanguagae.model_validate_json(data)

            db = SessionLocal()
            user.preferredLanguage = user.preferredLanguage.lower()
            db.query(models.UserInfo).filter(models.UserInfo.userId == user.userId).update({'preferredLanguage': user.preferredLanguage,'updatedAt':datetime.now()})
            db.commit()
            if user.isUrdu:
                msg = "زبان کو کامیابی کے ساتھ تبدیل کر دیا گیا ہے۔"
            else:
                msg = "Language Change Successfully"
            response_data = {
                "code": 200,
                "status": "Success",
                "message": msg,
                "data": {"message": msg}
            }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)




@app.post("/api/auth/v1/contactUs", response_model=EncryptResponseModel)
def contactus(payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            response_data = {
                "code": 200,
                "status": "Success",
                "message": "True",
                "data": {"email": "boost@gmail.com", "phone": "0321xxxxxxx"}
            }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)

@app.post("/api/auth/v1/howToApplyLoan", response_model=EncryptResponseModel)
def how_to_apply_loan(input:DecryptInput,payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = UserPreference.model_validate_json(data)

            if user.isUrdu:
                msg = "<html><head><title>لورم ایپسم</title></head><body><h2>لورم ایپسم</h2><p>لورم ایپسم ڈالر سٹ ایمٹ، کنسیکٹیٹر ایڈیپسکنگ ایلٹ۔ سیڈ وویرا ویسٹیبولم ایکس، ایک بائیبنڈم ایکس کونوالس نیک۔ پروئن ویہیکولا نسی سیڈ فاکوس بس۔ ووئمس لوبورٹس سیم ایگٹ ایکس رٹرم، نون فاکوس کون رونکس۔ پیلنٹسک ہبٹنٹ موربی ٹرسٹیک سینیکٹس ایٹ نیٹس ایٹ ملیسوآڈا فیمس ایک ٹرپس ایک ٹرپس ایٹ ترپیس۔ اٹ ویل ویریئس آرکو، ان ویریئس ڈولر۔ ووئمس ایوسموڈ سیمپر نیسی، ویل کونسیکوئٹ لوریم ایفکٹور نیک۔</p><p>ویسٹیبولم انٹے اپسم پرمس ان فاؤسبس اورسی لکٹس ایٹ اولٹریسز پوسویرے کیوبلیا کورے؛ پروئن ایوسموڈ ماسا سٹ ایمٹ اورنا بائیبنڈم، ایک فیوگیئٹ ایسٹ فنیبس۔ نلع فاسلیسی۔ ڈنک ٹنکڈنٹ، سیم نیک سیلرسکویس ہینڈریرٹ، اپسم نیسی کنسیکٹیٹور ویلٹ، ایڈ ویریئس لوریم لیبرو ویل فیلس۔ اٹ پلوینار، اوڈیو ویل ایوسموڈ ٹنسیڈنٹ، ٹیلس ننک ولپٹیٹ لیبرو، ویل ڈکٹم ایلٹ ارکو ان اوگو۔</p><h3>فوائد:</h3><ul><li>کیورابیٹر کنسیکٹور پورس ماسا</li><li>ایٹیم اُلمکورپر وولٹپٹ ٹارٹر</li><li>انٹیگر سولیسٹوڈن سیم نیک سولیسٹوڈن</li><li>ڈنیک ویل سیم انٹ اِیڈ کوئم سگٹس ایلیفینڈ</li><li>کوئسکو سگٹس ایلیٹ ویل کوئم ٹنسیڈنٹ</li></ul><h3>ختم:</h3><p>ان سیڈ ٹرسٹیک دوئی۔ سیڈ نیک اارکو سیڈ اُرنا ڈگنسیم فنیبس۔ نلع ایگٹ بائیبنڈم ایروس۔ الیکوآم ایرٹ وولٹپٹ۔ الیکوآم ایرٹ وولٹپٹ۔ سیڈ ایفکٹٹور، جسٹ ائو اکٹر انٹرڈم، ایرٹ جسٹ کونگو ایکس، سیڈ ویسٹیبولم لگلا ارکو سیڈ لیکٹس۔</p></body></html>"
            else:
                msg="<html><head><title>Lorem Ipsum</title></head><body><h2>Lorem Ipsum</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra vestibulum ex, ac bibendum ex convallis nec. Proin vehicula nisi sed faucibus. Vivamus lobortis sem eget ex rutrum, non faucibus quam rhoncus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut vel varius arcu, in varius dolor. Vivamus euismod semper nisi, vel consequat lorem efficitur nec.</p><p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Proin euismod massa sit amet urna bibendum, ac feugiat est finibus. Nulla facilisi. Donec tincidunt, sem nec scelerisque hendrerit, ipsum nisi consectetur velit, id varius lorem libero vel felis. Ut pulvinar, odio vel euismod tincidunt, tellus nunc vulputate libero, vel dictum elit arcu in augue.</p><h3>Benefits:</h3><ul><li>Curabitur consectetur purus massa</li><li>Etiam ullamcorper volutpat tortor</li><li>Integer sollicitudin sem nec sollicitudin</li><li>Donec vel sem id quam sagittis eleifend</li><li>Quisque sagittis elit vel quam tincidunt</li></ul><h3>Conclusion:</h3><p>In sed tristique dui. Sed nec arcu sed urna dignissim finibus. Nullam eget bibendum eros. Aliquam erat volutpat. Aliquam erat volutpat. Sed efficitur, justo eu auctor interdum, erat justo congue lacus, sed vestibulum ligula arcu sed lectus.</p></body></html>"
            response_data = {
                "code": 200,
                "status": "Success",
                "message": "True",
                "data": {"howToApplyLoan": msg}
            }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)


@app.post("/api/auth/v1/termsAndCondition", response_model=EncryptResponseModel)
def terms_and_condition(input:DecryptInput,payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = UserPreference.model_validate_json(data)
            if user.isUrdu:
                msg = "<html><head><title>لورم ایپسم</title></head><body><h2>لورم ایپسم</h2><p>لورم ایپسم ڈالر سٹ ایمٹ، کنسیکٹیٹر ایڈیپسکنگ ایلٹ۔ سیڈ وویرا ویسٹیبولم ایکس، ایک بائیبنڈم ایکس کونوالس نیک۔ پروئن ویہیکولا نسی سیڈ فاکوس بس۔ ووئمس لوبورٹس سیم ایگٹ ایکس رٹرم، نون فاکوس کون رونکس۔ پیلنٹسک ہبٹنٹ موربی ٹرسٹیک سینیکٹس ایٹ نیٹس ایٹ ملیسوآڈا فیمس ایک ٹرپس ایک ٹرپس ایٹ ترپیس۔ اٹ ویل ویریئس آرکو، ان ویریئس ڈولر۔ ووئمس ایوسموڈ سیمپر نیسی، ویل کونسیکوئٹ لوریم ایفکٹور نیک۔</p><p>ویسٹیبولم انٹے اپسم پرمس ان فاؤسبس اورسی لکٹس ایٹ اولٹریسز پوسویرے کیوبلیا کورے؛ پروئن ایوسموڈ ماسا سٹ ایمٹ اورنا بائیبنڈم، ایک فیوگیئٹ ایسٹ فنیبس۔ نلع فاسلیسی۔ ڈنک ٹنکڈنٹ، سیم نیک سیلرسکویس ہینڈریرٹ، اپسم نیسی کنسیکٹیٹور ویلٹ، ایڈ ویریئس لوریم لیبرو ویل فیلس۔ اٹ پلوینار، اوڈیو ویل ایوسموڈ ٹنسیڈنٹ، ٹیلس ننک ولپٹیٹ لیبرو، ویل ڈکٹم ایلٹ ارکو ان اوگو۔</p><h3>فوائد:</h3><ul><li>کیورابیٹر کنسیکٹور پورس ماسا</li><li>ایٹیم اُلمکورپر وولٹپٹ ٹارٹر</li><li>انٹیگر سولیسٹوڈن سیم نیک سولیسٹوڈن</li><li>ڈنیک ویل سیم انٹ اِیڈ کوئم سگٹس ایلیفینڈ</li><li>کوئسکو سگٹس ایلیٹ ویل کوئم ٹنسیڈنٹ</li></ul><h3>ختم:</h3><p>ان سیڈ ٹرسٹیک دوئی۔ سیڈ نیک اارکو سیڈ اُرنا ڈگنسیم فنیبس۔ نلع ایگٹ بائیبنڈم ایروس۔ الیکوآم ایرٹ وولٹپٹ۔ الیکوآم ایرٹ وولٹپٹ۔ سیڈ ایفکٹٹور، جسٹ ائو اکٹر انٹرڈم، ایرٹ جسٹ کونگو ایکس، سیڈ ویسٹیبولم لگلا ارکو سیڈ لیکٹس۔</p></body></html>"
            else:
                msg="<html><head><title>Lorem Ipsum</title></head><body><h2>Lorem Ipsum</h2><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra vestibulum ex, ac bibendum ex convallis nec. Proin vehicula nisi sed faucibus. Vivamus lobortis sem eget ex rutrum, non faucibus quam rhoncus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Ut vel varius arcu, in varius dolor. Vivamus euismod semper nisi, vel consequat lorem efficitur nec.</p><p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Proin euismod massa sit amet urna bibendum, ac feugiat est finibus. Nulla facilisi. Donec tincidunt, sem nec scelerisque hendrerit, ipsum nisi consectetur velit, id varius lorem libero vel felis. Ut pulvinar, odio vel euismod tincidunt, tellus nunc vulputate libero, vel dictum elit arcu in augue.</p><h3>Benefits:</h3><ul><li>Curabitur consectetur purus massa</li><li>Etiam ullamcorper volutpat tortor</li><li>Integer sollicitudin sem nec sollicitudin</li><li>Donec vel sem id quam sagittis eleifend</li><li>Quisque sagittis elit vel quam tincidunt</li></ul><h3>Conclusion:</h3><p>In sed tristique dui. Sed nec arcu sed urna dignissim finibus. Nullam eget bibendum eros. Aliquam erat volutpat. Aliquam erat volutpat. Sed efficitur, justo eu auctor interdum, erat justo congue lacus, sed vestibulum ligula arcu sed lectus.</p></body></html>"
            response_data = {
                "code": 200,
                "status": "Success",
                "message": "True",
                "data": {"termsAndCondition": msg}
            }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)



@app.post("/api/auth/v1/changePinOtp", response_model=EncryptResponseModel)
def change_pin_otp_request(input:DecryptInput, payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = UserLogout.model_validate_json(data)

            db = SessionLocal()
            otp = generate_otp()
            existing_user = db.query(models.UserInfo).filter(models.UserInfo.userId == user.userId).first()
            mobileNumber = "+92" + str(existing_user.mobileNumber)

            # write code to send the otp on mobile number

            hashed_otp = bcrypt.hashpw(otp.encode("utf-8"), bcrypt.gensalt())
            existing_user.otp = hashed_otp
            existing_user.updatedAt = datetime.now()
            db.commit()

            if user.isUrdu:
                msg = f"OTP +92{existing_user.mobileNumber} پر بھیجا گیا ہے "

            else:
                msg = f"OTP has been sent to +92{existing_user.mobileNumber}"

            response_data = {
                "code": 200,
                "status": "Success",
                "message": msg,
                "data": { "message": msg}
            }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)


@app.post("/api/auth/v1/verifyOtp", response_model=EncryptResponseModel)
def verify_otp(input:DecryptInput, payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = VerifyOTP.model_validate_json(data)

            db = SessionLocal()
            hashed_otp = db.query(models.UserInfo.otp).filter(models.UserInfo.userId == user.userId).first()
            #if bcrypt.checkpw(user.otp.encode("utf-8"), hashed_otp[0].encode("utf-8")):
            if user.isUrdu:
                msg = " کامیابی سے تصدیق ہو گئی"
            else:
                msg = "OTP Verified Successfully"

            response_data = {
                "code": 200,
                "status": "Success",
                "message": msg,
                "data": {"message": msg}
            }
            # else:
            #     if user.isUrdu:
            #         msg = "غلط OTP"
            #     else:
            #         msg = "Invalid OTP"
            #
            #     response_data = {
            #         "code": 400,
            #         "status": "Fail",
            #         "message": msg,
            #         "data": {}
            #     }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)


@app.post("/api/auth/v1/changePin", response_model=EncryptResponseModel)
def change_pin(input:DecryptInput, payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = ChangePin.model_validate_json(data)

            db = SessionLocal()
            hashed_pin = bcrypt.hashpw(user.pin.encode("utf-8"), bcrypt.gensalt())
            db.query(models.UserInfo).filter(models.UserInfo.userId == user.userId).update({'pin': hashed_pin, 'updatedAt': datetime.now()})
            db.commit()

            if user.isUrdu:
                msg = " پن کامیابی سے تبدیل کر دی گئی ہے۔"
            else:
                msg = "pin changed successfully"

            response_data = {
                "code": 200,
                "status": "Success",
                "message": msg,
                "data": {"message": msg}
            }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)



@app.post("/api/auth/v1/UserKyc", response_model=EncryptResponseModel)
async def user_kyc(request: Request,
                   userId: str = Form(),
                   isUrdu: str = Form(),
                   education: str = Form(),
                   address: str = Form(),
                   maritalStatus: str = Form(),
                   children: str = Form(None),
                   employmentStatus: str = Form(),
                   studentType: str = Form(None),
                   instituteName: str = Form(None),
                   enrollmentDate: str = Form(None),
                   studentId: UploadFile = File(None),
                   companyName: str = Form(None),
                   dateOfJoining: str = Form(None) ,
                   designation: str = Form(None),
                   salarySlip: UploadFile = File(None),
                   cnicPicture: UploadFile = File(),
                   cnicNumber: str = Form(),
                   cnicDateOfBirth: str = Form(),
                   cnicDateOfExpiry: str = Form(),
                   facePicture: UploadFile = File(),
                   payload: dict = Depends(validate_token)):

    is_valid, resp = payload
    if is_valid:
        try:
            userId = decrypt_and_decode(userId)
            isUrdu = decrypt_and_decode(isUrdu)
            education = decrypt_and_decode(education)
            address = decrypt_and_decode(address)
            maritalStatus = decrypt_and_decode(maritalStatus)
            employmentStatus = decrypt_and_decode(employmentStatus)
            cnicNumber = decrypt_and_decode(cnicNumber)
            cnicDateOfBirth = decrypt_and_decode(cnicDateOfBirth)
            cnicDateOfExpiry = decrypt_and_decode(cnicDateOfExpiry)
            if children:
                children = decrypt_and_decode(children)
            if studentType:
                studentType = decrypt_and_decode(studentType)
            if instituteName:
                instituteName = decrypt_and_decode(instituteName)
            if enrollmentDate:
                enrollmentDate = decrypt_and_decode(enrollmentDate)
            if companyName:
                companyName = decrypt_and_decode(companyName)
            if dateOfJoining:
                dateOfJoining =decrypt_and_decode(dateOfJoining)
            if designation:
                designation = decrypt_and_decode(designation)

            user = UserKyc(userId=userId,isUrdu = isUrdu,education=education,address=address,
                           maritalStatus=maritalStatus,children=children,employmentStatus=employmentStatus,
                           studentType=studentType,instituteName=instituteName,enrollmentDate=enrollmentDate,
                           companyName=companyName,designation=designation,dateOfJoining=dateOfJoining,cnicNumber=cnicNumber,
                           cnicDateOfBirth=cnicDateOfBirth,cnicDateOfExpiry=cnicDateOfExpiry)

            mandatory_field = ''
            if user.maritalStatus.lower() != 'single' and not user.children:
                mandatory_field = "children"

            elif user.employmentStatus.lower() == 'student':
                if user.studentType is None or not user.studentType:
                    mandatory_field = "studentType"
                if user.instituteName is None or not user.instituteName:
                    mandatory_field = "instituteName"
                if user.enrollmentDate is None or not user.enrollmentDate:
                    mandatory_field = "enrollmentDate"
                if not studentId.filename:
                    mandatory_field = "studentId"

            elif employmentStatus.lower() in ["employed", "self employed"]:
                   if user.companyName is None or not user.companyName:
                       mandatory_field = "companyName"
                   if user.dateOfJoining is None or not user.dateOfJoining:
                       mandatory_field = "dateOfJoining"
                   if user.designation is None or not user.designation:
                       mandatory_field = "designation"
                   if not salarySlip.filename:
                       mandatory_field = "salarySlip"


            if mandatory_field:
                response_data = {
                    "code": 400,
                    "status": "Fail",
                    "message": f"The value of '{mandatory_field}' is required",
                    "data": {}
                }
                encrypt_response = encrypt_data(response_data)
                return JSONResponse(content=encrypt_response, status_code=400)


            db = SessionLocal()

            now = datetime.now()
            formattedDate = now.strftime("%Y-%m-%d_%H-%M-%S")

            cnicFileName = cnicPicture.filename
            cnicFileExtension = cnicFileName.rsplit('.', 1)[-1]
            cnicDirectory = "UserCNIC"
            cnicFileName = str(user.userId) + "_cinc_" + formattedDate + "." + cnicFileExtension
            os.makedirs(cnicDirectory, exist_ok=True)
            cnicFilePath = os.path.join(cnicDirectory, cnicFileName)
            await save_uploaded_file(cnicPicture, cnicFilePath)


            faceFileName = facePicture.filename
            faceFileExtension = faceFileName.rsplit('.', 1)[-1]
            faceDirectory = "FacePictures"
            faceFileName = str(user.userId) + "_face_" + formattedDate + "." + faceFileExtension
            os.makedirs(faceDirectory, exist_ok=True)
            faceFilePath = os.path.join(faceDirectory, faceFileName)
            await save_uploaded_file(facePicture, faceFilePath)

            users_kyc = models.UserKYC(
                userId=user.userId,
                employmentType = user.employmentStatus,
                qualification=user.education,
                address=user.address,
                maritalStatus=user.maritalStatus,
                children=user.children,
                cnicPic=cnicFileName,
                cnicNo=user.cnicNumber,
                dateOfBirth=user.cnicDateOfBirth,
                dateOfCnicExpiry=user.cnicDateOfExpiry,
                facePicture=faceFileName,
                createdAt=datetime.now()
            )
            db.add(users_kyc)
            db.commit()

            if user.employmentStatus.lower() == 'student':
                studentFileName = studentId.filename
                studentFileExtension = studentFileName.rsplit('.', 1)[-1]
                studentDirectory = "StudentCards"
                studentFileName = str(user.userId) + "_studentId_" + formattedDate + "." + studentFileExtension
                os.makedirs(studentDirectory, exist_ok=True)
                studentFilePath = os.path.join(studentDirectory, studentFileName)
                await save_uploaded_file(studentId, studentFilePath)

                student_info = models.StudentInfo(
                    kycId=users_kyc.kycId,
                    studentType=user.studentType,
                    instituteName=user.instituteName,
                    enrolmentDate=user.enrollmentDate,
                    studentCardPic=studentFileName,
                    createdAt=datetime.now()
                )
                db.add(student_info)
                db.commit()

            elif employmentStatus.lower() in ["employed", "self employed"]:
                employeeFileName = salarySlip.filename
                employeeFileExtension = employeeFileName.rsplit('.', 1)[-1]
                employeeDirectory = "SalarySlip"
                employeeFileName = str(user.userId) + "_salarySlip_" + formattedDate + "." + employeeFileExtension
                os.makedirs(employeeDirectory, exist_ok=True)
                employeeFilePath = os.path.join(employeeDirectory, employeeFileName)
                await save_uploaded_file(salarySlip, employeeFilePath)

                employment_details = models.EmploymentDetails(
                    kycId=users_kyc.kycId,
                    companyName=user.companyName,
                    designation=user.designation,
                    dateOfJoining=user.dateOfJoining,
                    salarySlipPic=employeeFileName,
                    createdAt=datetime.now()
                )
                db.add(employment_details)
                db.commit()

            if user.isUrdu:
                msg = "KYC کامیابی سے مکمل ہوگیا ہے"
            else:
                msg = "KYC Completed Sucessfully"

            response_data = {
                "code": 200,
                "status": "Success",
                "message": msg,
                "data": {"message": msg}}

        except Exception as e:
            response_data ={
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)


    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)


@app.post("/api/auth/v1/bankList", response_model=EncryptResponseModel)
def get_bank_list(request:Request,payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            db = SessionLocal()
            bank_list = db.query(models.BankName).all()
            bank_lists = [{"bankId": bank.bankId, "bankName": bank.bankName,"bankImage":str(request.base_url) + "BankLogo/" + bank.bankImage} for bank in bank_list]
            response_data = {
                "code": 200,
                "status": "Success",
                "message": True,
                "data": {"BankList":bank_lists}
            }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)


@app.post("/api/auth/v1/loanAmount", response_model=EncryptResponseModel)
def loan_amount(input:DecryptInput, payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = LoanAmount.model_validate_json(data)

            today = datetime.today()
            thirty_days = (today + timedelta(days=30)).strftime('%Y-%m-%d')
            sixty_days = (today + timedelta(days=60)).strftime('%Y-%m-%d')
            ninty_days = (today + timedelta(days=90)).strftime('%Y-%m-%d')


            LoanCycle =[{"days": 30, "DueDate":thirty_days,"serviceFee":int(user.amount * 0.02)},
            {"days": 60, "DueDate": sixty_days, "serviceFee": int(user.amount * 0.04)},
            {"days": 90, "DueDate": ninty_days, "serviceFee": int(user.amount * 0.08)}]

            db = SessionLocal()
            result = db.query(models.BankName.bankName,models.AccountDetails.accountDetailId, models.AccountDetails.accountTitle, models.AccountDetails.IBAN) .join(models.AccountDetails, models.BankName.bankId == models.AccountDetails.bankId).filter(models.AccountDetails.userId == user.userId).all()

            BankDetails = [{"accountId":account_id, "bankName": bank_name,"accountTitle": account_title, "IBAN": iban} for bank_name,account_id, account_title, iban in result]

            response_data = {
                "code": 200,
                "status": "Success",
                "message": True,
                "data": {"LoanCycle":LoanCycle, "UserBankDetails":BankDetails}
            }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)


@app.post("/api/auth/v1/submitLoanApplication", response_model=EncryptResponseModel)
def submit_loan(input:DecryptInput, payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = SubmitLoan.model_validate_json(data)


            db = SessionLocal()
            if user.daysToReturnLoan == 30:
                servicepercentage = 0.02
            elif user.daysToReturnLoan == 60:
                servicepercentage = 0.04
            else:
                servicepercentage = 0.08
            serviceFee = int(user.amount * servicepercentage)
            repayableAmount = user.amount + serviceFee

            if user.accountId:
                borrow_loan = models.BorrowLoan(
                    accountDetailId=user.accountId,
                    amountBorrow=user.amount,
                    repayableAmount=repayableAmount,
                    applicationDate=datetime.now().date(),
                    repaymentDays=user.daysToReturnLoan,
                    status='pending',
                    isReleased = 0,
                    createdAt=datetime.now()
                )
                db.add(borrow_loan)
                db.commit()

                if user.isUrdu:
                    msg = "درخواست کامیابی کے ساتھ جمع کر دی گئی ہے۔"
                else:
                    msg = "Application Sumbmitted Sucessfully"
                response_data = {
                    "code": 200,
                    "status": "Success",
                    "message": msg,
                    "data": {"message": msg}
                }

            elif user.bankId and user.amount and user.ibanNumber:
                account_details = models.AccountDetails(
                    userId=user.userId,
                    bankId=user.bankId,
                    accountTitle=user.accountTitle,
                    IBAN=user.ibanNumber,
                    createdAt=datetime.now()
                )
                db.add(account_details)
                db.commit()

                borrow_loan = models.BorrowLoan(
                    accountDetailId=account_details.accountDetailId,
                    amountBorrow=user.amount,
                    repayableAmount=repayableAmount,
                    applicationDate=datetime.now().date(),
                    repaymentDays=user.daysToReturnLoan,
                    status='pending',
                    isReleased=0,
                    createdAt=datetime.now()
                )
                db.add(borrow_loan)
                db.commit()

                if user.isUrdu:
                    msg = "درخواست کامیابی کے ساتھ جمع کر دی گئی ہے۔"
                else:
                    msg = "Application Sumbmitted Sucessfully"
                response_data = {
                    "code": 200,
                    "status": "Success",
                    "message": msg,
                    "data": {"message": msg}
                }
            else:
                if user.isUrdu:
                    msg = "لازمی فیلڈز غائب ہیں۔"
                else:
                    msg = "mandatory fields are missing"
                response_data = {
                    "code": 400,
                    "status": "Fail",
                    "message": msg,
                    "data": {}
                }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)


@app.post("/api/auth/v1/getLoanDetails", response_model=EncryptResponseModel)
def loan_details(input:DecryptInput, payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = LoanDetail.model_validate_json(data)

            db = SessionLocal()
            result = db.query(models.BorrowLoan.loanId, models.BorrowLoan.repayableAmount, models.BorrowLoan.repaymentDays, models.BorrowLoan.repaymentDate, case((and_(models.BorrowLoan.status == 'paid', models.BorrowLoan.isReleased == 1, models.PayBack.payBackStatus.is_(None), models.BorrowLoan.repaymentDate >= func.CURRENT_DATE()), 'due'), (and_(models.BorrowLoan.status == 'paid', models.BorrowLoan.isReleased == 1, models.PayBack.payBackStatus.is_(None), models.BorrowLoan.repaymentDate < func.CURRENT_DATE()), 'overdue'), (and_(models.BorrowLoan.status == 'paid', models.BorrowLoan.isReleased == 1, models.PayBack.payBackStatus == 'pending'), 'ongoing'), (and_(models.BorrowLoan.status == 'paid', models.BorrowLoan.isReleased == 1, models.PayBack.payBackStatus == 'paid'), 'completed'), (models.BorrowLoan.status == 'reject', 'reject'), (models.BorrowLoan.status == 'pending', 'waiting for approval'), else_='').label('loanStatus')).join(models.AccountDetails, models.BorrowLoan.accountDetailId == models.AccountDetails.accountDetailId).outerjoin(models.PayBack, models.PayBack.loanId == models.BorrowLoan.loanId).filter(models.AccountDetails.userId == user.userId).all()
            LoanDetails = []
            for loanId, repayableAmount,repaymentDays, repaymentDate, status in result:
                if repaymentDate:
                    repaymentDate = repaymentDate.strftime('%Y-%m-%d')
                else:
                    repaymentDate = None


                loan_details= {"loanId": loanId,
                 "repayableAmount": repayableAmount,
                 "repaymentDate": repaymentDate,
                "days" : repaymentDays,
                 "status": status}

                LoanDetails.append(loan_details)


            response_data = {
                "code": 200,
                "status": "Success",
                "message": True,
                "data": {"LoanDetails": LoanDetails}
            }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)



@app.post("/api/auth/v1/getDueDateLoanDetails", response_model=EncryptResponseModel)
def loan_details(input:DecryptInput, payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            data = decrypt_input(input.data)
            data = json.loads(data.body.decode())
            data = json.dumps(data)
            user = LoanDetail.model_validate_json(data)

            db = SessionLocal()
            subquery = db.query(models.BorrowLoan.loanId, models.BorrowLoan.repayableAmount, models.BorrowLoan.repaymentDays, models.BorrowLoan.repaymentDate, case((and_(models.BorrowLoan.status == 'paid', models.BorrowLoan.isReleased == 1, models.PayBack.payBackStatus.is_(None), models.BorrowLoan.repaymentDate >= func.CURRENT_DATE()), 'due'), (and_(models.BorrowLoan.status == 'paid', models.BorrowLoan.isReleased == 1, models.PayBack.payBackStatus.is_(None), models.BorrowLoan.repaymentDate < func.CURRENT_DATE()), 'overdue')).label('loanStatus')).join(models.AccountDetails, models.BorrowLoan.accountDetailId == models.AccountDetails.accountDetailId).outerjoin(models.PayBack, models.PayBack.loanId == models.BorrowLoan.loanId).filter(models.AccountDetails.userId == user.userId).subquery()
            result = (db.query(subquery.c.loanId, subquery.c.repayableAmount, subquery.c.repaymentDays,subquery.c.repaymentDate, subquery.c.loanStatus).filter(subquery.c.loanStatus.isnot(None)).all())

            LoanDetails = []
            for loanId, repayableAmount,repaymentDays, repaymentDate, status in result:
                if repaymentDate:
                    repaymentDate = repaymentDate.strftime('%Y-%m-%d')
                else:
                    repaymentDate = None


                loan_details= {"loanId": loanId,
                 "repayableAmount": repayableAmount,
                 "repaymentDate": repaymentDate,
                "days" : repaymentDays,
                 "status": status}

                LoanDetails.append(loan_details)


            response_data = {
                "code": 200,
                "status": "Success",
                "message": True,
                "data": {"LoanDetails": LoanDetails}
            }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)



@app.post("/api/auth/v1/payBack", response_model=EncryptResponseModel)
async def payback_loan(request: Request,
                   loanId: str = Form(),isUrdu: str = Form(),payReceipt: UploadFile = File(), payload: dict = Depends(validate_token)):
    is_valid, resp = payload
    if is_valid:
        try:
            loanId = decrypt_and_decode(loanId)
            isUrdu = decrypt_and_decode(isUrdu)

            user =PayBack(loanId=loanId,isUrdu=isUrdu)
            if not payReceipt.filename:
                if user.isUrdu:
                    msg = "ادائیگی کی رسید درکار ہے۔"
                else:
                    msg ="pay receipt is Required"
                response_data = {
                    "code": 400,
                    "status": "Fail",
                    "message": msg,
                    "data": {}
                }
                encrypt_response = encrypt_data(response_data)
                return JSONResponse(content=encrypt_response, status_code=400)

            now = datetime.now()
            formattedDate = now.strftime("%Y-%m-%d_%H-%M-%S")

            receiptFileName = payReceipt.filename
            receiptFileExtension = receiptFileName.rsplit('.', 1)[-1]
            receiptDirectory = "PayReceipt"
            receiptFileName = str(user.loanId) + "_receipt_" + formattedDate + "." + receiptFileExtension
            os.makedirs(receiptDirectory, exist_ok=True)
            receiptFilePath = os.path.join(receiptDirectory, receiptFileName)
            await save_uploaded_file(payReceipt, receiptFilePath)
            db = SessionLocal()

            pay_back = models.PayBack(
                loanId=user.loanId,
                payBackStatus='pending',
                receiptPic=receiptFileName,
                paidAt=datetime.now(),
                createdAt=datetime.now()
            )
            db.add(pay_back)
            db.commit()

            if user.isUrdu:
                msg = "آپ نے اپنے قرض کی واپسی کی درخواست کامیابی کے ساتھ جمع کرادی ہے"
            else:
                msg = "You have successfully submitted your loan repayment request"
            response_data = {
                "code": 200,
                "status": "Success",
                "message": msg,
                "data": {"message": msg}
            }

        except Exception as e:
            response_data = {
                "code": 400,
                "status": "Fail",
                "message": str(e),
                "data": {}
            }
            encrypt_response = encrypt_data(response_data)
            return JSONResponse(content=encrypt_response, status_code=400)
    else:
        response_data = {
            "code": 401,
            "status": "Fail",
            "message": resp,
            "data": {}
        }
    code = response_data["code"]
    encrypt_response = encrypt_data(response_data)
    return JSONResponse(content=encrypt_response, status_code=code)

if __name__ == "__main__":
    uvicorn.run(app, host="127.0.0.1", port=8000)
